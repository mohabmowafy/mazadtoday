//
//  DetailsCarVC.swift
//  TZONE
//
//  Created by lapstore on 12/25/18.
//  Copyright © 2018 AmrSobhy. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class DetailsCarVC: UIViewController {

    @IBOutlet weak var ViewContainImage: UIView!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var PriceBaby: UILabel!
    @IBOutlet weak var BabySeat: UILabel!
    @IBOutlet weak var PriceChild: UILabel!
    @IBOutlet weak var ChildSeat: UILabel!
    @IBOutlet weak var PriceAdditinal: UILabel!
    @IBOutlet weak var Addetional: UILabel!
    @IBOutlet weak var PriceNav: UILabel!
    @IBOutlet weak var GPSNavigation: UILabel!
    @IBOutlet weak var CarDoors: UILabel!
    @IBOutlet weak var CarGray: UILabel!
    @IBOutlet weak var CarNew: UILabel!
    @IBOutlet weak var CarYear: UILabel!
    @IBOutlet weak var CarType: UILabel!
    @IBOutlet weak var PriceDayLabel: UILabel!
    @IBOutlet weak var PriceLabel: UILabel!
    @IBOutlet weak var GreatLabel: UILabel!
    @IBOutlet weak var CarImage: UIImageView!
    @IBOutlet weak var BookThisCar: UIButton!
    @IBOutlet weak var ViewContainRadio: UIView!
    
    @IBOutlet weak var AdditionalDriver: UIButton!
    
    @IBOutlet weak var GPS_Navig: UIButton!
    @IBOutlet weak var childse: UIButton!
    
    @IBOutlet weak var bodyse: UIButton!
    
    var getAll = [GetAll]()
    var car_id = String()
    var RaOne = String()
    var RaTwo = String()
    var roThree = String()
    var roFour = String()
    var orderId = ""
    var driver : Bool = false
    var additional_specification = [String]()
     var check = false
    var Mybooki = [MyBookingModel]()
    var carId  = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        print("car id : ", car_id)
        // Do any additional setup after loading the view.
        self.ViewContainImage.applyShadow()
        self.ViewContainImage.roundCorners(cornerRadius: 10)
        self.ViewContainRadio.applyShadow()
        self.ViewContainRadio.roundCorners(cornerRadius: 10)
        self.BookThisCar.roundCorners(cornerRadius: 25)
        self.get_all()
        get_data(Mybooki: Mybooki)
    
    }
   
    
    func get_data(Mybooki :[MyBookingModel]){
        
        self.AdditionalDriver.isSelected = driver
        
        if Mybooki.count > 0 {
            for selectedBooking in Mybooki{
                if selectedBooking.id_order == self.orderId{
                    self.setUpViewElement(carDetailsFromModel: selectedBooking.Car_Deatiles!)
                    self.BookThisCar.setTitle("More Information", for: .normal)
                    for selected in selectedBooking.additional_specification!{
                        switch selected{
                        case "GPS Navigation":
                            self.GPS_Navig.isSelected = true
                        case "Child Seat" :
                            self.childse.isSelected = true
                        case "Baby Seat":
                            self.bodyse.isSelected = true
                        case "Additional Driver":
                            self.AdditionalDriver.isSelected = true
                        default:
                            print("empty Addtion")
                        }
                    }
                    
                }
            }
        }else{
            getCarInformations { (cardata, success) in
                if success{
                    if let data = cardata{
                        self.setUpViewElement(carDetailsFromModel: data)
                    }
                
                }else{
                    print("no data for car")
                }
            }
        }
        
    }

    //MARK:- get car informations by id
    func getCarInformations(complation : @escaping (_ dataOfCar : DetailsCarModelById?, _ status: Bool )-> Void){
        let url = "http://prosolutions-it.com/tajjer/json/car_detailes.php?ID=\(car_id)"
         let encodedUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print(encodedUrl)
        Alamofire.request(encodedUrl!).responseJSON { (respond) in
            switch respond.result{
            case .failure(let error):
                print(error)
                complation(nil, false)
            case .success(let value):
                print("car dateails Using Alamo \(value)")
                
               let data = JSON(value)
                guard let car_Detai = data.dictionary else{
                    return
                }
                
                let ID = (car_Detai["ID"]?.string) ?? ""
                self.car_id = ID
                let Diesel = (car_Detai["Diesel"]?.string) ?? ""
                let bags = car_Detai["bags"]?.string ?? ""
                let Doors = car_Detai["Doors"]?.string ?? ""
                let made_year = car_Detai["made_year"]?.string ?? ""
                let main_img = car_Detai["main_img"]?.string ?? ""
                let price = car_Detai["price"]?.string ?? ""
                let price_offer = car_Detai["price_offer"]?.string ?? ""
                let type_Motor = car_Detai["type_Motor"]?.string ?? ""
                let Speed = car_Detai["Speed"]?.string ?? ""
                let title_car = car_Detai["title_car"]?.string ?? ""
                let Images = car_Detai["images"]?.array
                let Power_Motor = car_Detai["Power_Motor"]?.string ?? ""
                var imageAr = [String]()
                var count = 0
                for image in Images!{
                    if let imag =  image["img\(count)"].string {
                        imageAr.append(imag)
                        count = count + 1
                    }
                    
                }
                let carDat = DetailsCarModelById.init(title_car: title_car, main_img: main_img, images: imageAr, iD: ID, price: price, price_offer: price_offer, made_year: made_year, diesel: Diesel, type_Motor: type_Motor, power_Motor: Power_Motor, doors: Doors, bags: bags, speed: Speed, total_pages: bags)
                complation(carDat, true)
            }
        }
       
    }
    
    //MARK:- setup view Element
    func setUpViewElement(carDetailsFromModel: DetailsCarModelById){
        if let title = carDetailsFromModel.title_car{
            TitleLabel.text = title
            CarType.text = title
        }
        if let price = carDetailsFromModel.price{
            PriceLabel.text = "$\(price)"
        }
        if let offerDay = carDetailsFromModel.price_offer{
            PriceDayLabel.text = "$\(offerDay)/Day"
        }
        
        if let image = carDetailsFromModel.main_img{
            DispatchQueue.main.async {
                self.CarImage.kf.setImage(with: URL(string: image))
                self.CarImage.kf.indicatorType = .activity
            }
            
        }
        if let doors = carDetailsFromModel.doors{
            CarDoors.text = doors
        }
        if let speed = carDetailsFromModel.speed{
            CarNew.text = speed
        }
        if let carModel = carDetailsFromModel.made_year{
            CarYear.text = carModel
        }
        
    }
    
    //MARK:- additional specification add to car if i want
    func get_all(){
        let url = Globals.get_all
        let param = "type=1"
        Helper.POSTString(url: url, parameters: param) { (json) in
            print("ffdfdfdfdfd",json)
            self.getAll = ParseManager.ParseGetAll(array: json)
            DispatchQueue.main.async {
                if self.getAll.count > 0 {
                self.GPSNavigation.text = self.getAll[0].name
                self.PriceNav.text = "$" + self.getAll[0].price + "/Day"
                self.ChildSeat.text = self.getAll[1].name
                self.PriceChild.text = "$" + self.getAll[1].price + "/Day"
                self.Addetional.text = self.getAll[2].name
                self.PriceAdditinal.text = "$" + self.getAll[2].price + "/Day"
                self.BabySeat.text = self.getAll[3].name
                self.PriceBaby.text = "$" + self.getAll[3].price + "/Day"

                }
            }
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func RadioButtonAction(_ sender: Any) {
        if self.getAll.count > 0 {
            print("RaOne", self.getAll[0].id)
            self.RaOne = self.getAll[0].id
        }
    }
    
    @IBAction func RadioButtonTwoAction(_ sender: Any) {
        if self.getAll.count > 0 {
            print(self.getAll[2].id)
            self.RaTwo = self.getAll[2].id
        }

        
    }
    
    @IBAction func RadioButtonThreeAction(_ sender: Any) {
        if self.getAll.count > 0 {
            print(self.getAll[1].id)
            self.RaOne = self.getAll[1].id
//            self.roThree = self.getAll[1].id
        }
    }
    
    
    @IBAction func RadioButtonFourAction(_ sender: Any) {
        if self.getAll.count > 0 {
            print(self.getAll[3].id)
            self.RaTwo = self.getAll[3].id
            //self.roFour = self.getAll[3].id
        }
    }
    
    
    
    @IBAction func CancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func BookCar(_ sender: Any) {
        if Mybooki.count > 0{
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "InformationVC") as? InformationVC{
                vc.orderIdInformation = self.orderId
                vc.myBookInformation = self.Mybooki
                
            }
            self.performSegue(withIdentifier: "OpenInformation", sender: nil)
            
        }else{
            orderCar { (success, orderId, message) in
                if success{
                    
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "InformationVC") as? InformationVC{
                        vc.orderIdInformation = orderId!
                        vc.myBookInformation = self.Mybooki
                        
                    }
                    self.performSegue(withIdentifier: "OpenInformation", sender: nil)
                    
                }
            }
        }
    }
}

extension DetailsCarVC {
    func orderCar(complation:@escaping (_ status : Bool , _ order_id : String?,_ massage: String?)-> Void){
        let mem_id = DataManager().callData("ID")
        let url = "http://fmdscu.com/tajjer/json/order.php?mem_id=\(mem_id)&car_id=\(car_id)&Picking_Up_Date=2019-09-10&Droping_off_Date=2019-09-15&Picking_Up_Time=9:00pm&Droping_off_Time=10:00pm&Picking_Up_locations=bbbbbbb&Droping_off_locations=cccccccccccccc&additional_specification=[\"1\"]"
        
        let encodedUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        Alamofire.request(encodedUrl!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (Respond) in
            switch Respond.result{
            case .failure(let error):
                print(error)
                complation(false, nil,nil)
                self.showError("there are some error chuck your connaction", "Error")
            case .success(let value):
                let json = JSON(value)
                let order_Id = json["order_id"].stringValue
                let message = json["message"].stringValue
                
                complation(true, order_Id, message)
            }
        }
        
        
    }
}
